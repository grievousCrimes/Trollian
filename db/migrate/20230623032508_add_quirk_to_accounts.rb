class AddQuirkToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :quirk, :string, if_not_exists: true
  end
end
